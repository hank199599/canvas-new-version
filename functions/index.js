const functions = require('firebase-functions');
const {
  dialogflow,
  HtmlResponse
} = require('actions-on-google');

const firebaseConfig = JSON.parse(process.env.FIREBASE_CONFIG);

const app = dialogflow({
  debug: true
});

// TODO: Write your code here.
const judgeMap = {
  石頭: {
    石頭: '平手',
    布: '你輸了',
    剪刀: '你贏了!'
  },
  布: {
    石頭: '你贏了!',
    布: '平手',
    剪刀: '你輸了'
  },
  剪刀: {
    石頭: '你輸了',
    布: '你贏了!',
    剪刀: '平手'
  }
};

app.intent('Default Welcome Intent', conv => {
  conv.ask('你想選擇哪一個? 石頭? 布? 或是剪刀?');
  conv.ask(new HtmlResponse({
    url: `https://${firebaseConfig.projectId}.firebaseapp.com/`
  }));
});

app.intent('Show', (conv, param) => {
  // Retrieve the user's hand.
  const userChoice = param['user-choice'].toLowerCase();
  // Determine the action's hand in random order.
  const actionChoice = ['石頭', '布', '剪刀'][Math.floor(Math.random() * 3)];
  // Get the message represents the duel.
  const message = judgeMap[userChoice][actionChoice];
  // Construct the reply message with SSML.
  const ssml = `
    <speak>
      <p>Ok,我也想好我要出什麼了</p>
      <p>剪刀、石頭、布!</p>
      <p>你出的是 ${userChoice}.</p>
      <p>我出的是 ${actionChoice}.</p>
      <p>${message}</p>
      <break time="400ms" />
      <p>你想再玩一次嗎?</p>
    </speak>`;
  conv.ask(ssml);
  // HtmlResponse object with information to update the screen.
  conv.ask(new HtmlResponse({
    url: `https://${firebaseConfig.projectId}.firebaseapp.com/`,
    data: {
      scene: 'result',
      userChoice,
      actionChoice,
      message
    }
  }));
});

app.intent('數字進度條', (conv,{number}) => {
  conv.ask('好的，我已經調整長度到'+number);
  conv.ask(new HtmlResponse({
    url: `https://${firebaseConfig.projectId}.firebaseapp.com/numbers`,
	data: {
      inputs: number
    }
  }));
});
const SelectContexts = {parameter: 'count'}	

app.intent('進度條初始化', (conv,{number}) => {
  conv.ask(`<speak><p><s>接下來，我會計算你跟我對話的次數。</s><s>從現在開始算起，現在是第一次</s></p></speak>`);
  conv.ask(new HtmlResponse({
    url: `https://${firebaseConfig.projectId}.firebaseapp.com/prograss`,
	data: {
      count: 10
    }
  }));
  
  conv.user.storage.count=1;
});

app.intent('進度條測試', (conv,{number}) => {
  if(conv.input.raw.indexOf('結束')===-1){
	conv.user.storage.count=conv.user.storage.count+1;  
    conv.contexts.set(SelectContexts.parameter, 1)
  conv.ask(`<speak><p><s>現在</s><s>這是你跟我對話的第${conv.user.storage.count}次</s></p></speak>`);
  conv.ask(new HtmlResponse({
    url: `https://${firebaseConfig.projectId}.firebaseapp.com/prograss`,
	data: {
      count:conv.user.storage.count*10
    }
  }));
  }else{
  conv.ask('好的，我們回到猜拳遊戲了!');
  conv.ask(new HtmlResponse({
    url: `https://${firebaseConfig.projectId}.firebaseapp.com/`,
  }));
  }
});

app.intent('Default Fallback Intent', (conv,{number}) => {
  conv.ask('抱歉，麻煩你再說一次');
  conv.ask(new HtmlResponse({
    url: `https://${firebaseConfig.projectId}.firebaseapp.com/`,
  }));
});

app.intent('Show - yes', conv => {
  conv.ask('Ok. 你想選擇哪一個? 石頭? 布? 或, 剪刀?');
  conv.ask(new HtmlResponse({
    data: {
      scene: 'restart'
    }
  }));
});


exports.canvas_test = functions.https.onRequest(app);